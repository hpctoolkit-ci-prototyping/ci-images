#!/bin/bash -e

# Update the cached Spack repo
if ! [ -d cache/spack.git ]; then
  git clone --bare --depth=10 --single-branch --no-tags --branch=develop \
    https://github.com/spack/spack.git cache/spack.git
fi
git -C cache/spack.git fetch --verbose

# Expand to a proper temporary Spack instance
git -C cache/spack.git archive --format=tar --prefix=spack/ HEAD | tar -C /tmp -x
export PATH=/tmp/spack/bin:"$PATH"

# Check that bootstrapping works as it should
if ! STAT=$(spack bootstrap status --optional); then
  echo "Checking bootstrap status failed"
  exit 1
fi
if grep -q '\[FAIL\]' <<<"$STAT"; then
  echo "Some bootstrapping failed!"
  exit 1
fi

# Function to check whether a compiler is properly registered with Spack
# Usage: check_cc <Spack compiler spec> <compiler path regex>
function check_cc() {
  # Check that Spack knows about this compiler
  if ! spack compiler info "$1"; then
    echo "Failed to get compiler info for $1"
    return 1
  fi
  INFO=$(spack compiler info "$1")
  # Check that Spack knows about exactly 1 of this compiler
  if ! [ "$(grep -c '^[^[:space:]]' <<<"$INFO")" -eq 1 ]; then
    echo "Invalid number of compilers matching $1: expected 1, got $(grep -c '^[^[:space:]]' <<<"$INFO")"
    return 1
  fi
  # Check that Spack has appropriate paths to this compiler
  if grep / <<<"$INFO" | grep -qv "= $2"; then
    echo "Invalid paths in compiler $1:"
    grep / <<<"$INFO" | grep -v "= $2"
    return 1
  fi
  # Check that Spack has the appropriate OS for this compiler
  if ! grep "operating system" <<<"$INFO" | grep -q "[[:space:]]$(spack arch --operating-system)$"; then
    echo "Invalid OS code in compiler $1: expected $(spack arch --operating-system), got $(grep "operating system" <<<"$INFO")"
    return 1
  fi
}

# Check that all the compilers requested are in the right places
for spec in "$@"; do
  check_cc "${spec%%=*}" "${spec##*=}"
done

# Check that every compiler actually works
for c in $(spack compiler list | grep '@'); do
  if ! spack install zlib %"$c"; then
    echo "Failed to install zlib %$c"
    exit 1
  fi
done

# Regenerate the list of compilers via spack compiler find
mkdir /tmp/spack-cc-env
cat > /tmp/spack-cc-env/spack.yaml <<'EOF'
spack:
  compilers:: []
EOF
if spack -e /tmp/spack-cc-env/ config get compilers | grep 'spec:'; then
  echo "Environmental compiler override failed!"
  exit 1
fi
spack config get compilers | grep '^[[:space:]]*cc:' | cut -d: -f2- | sort -u | xargs spack -e /tmp/spack-cc-env/ compiler find
spack -e /tmp/spack-cc-env/ compiler find

# Verify that the regenerated specs are a subset of the recorded specs
diff -u <(spack compiler list) <(spack -e /tmp/spack-cc-env/ compiler list) > /tmp/spack-cc-env/cc.patch ||:
grep -vE '^(---|\+\+\+|@@)' /tmp/spack-cc-env/cc.patch > /tmp/spack-cc-env/cc.diff ||:
if grep -q '^[+-][^@]*$' /tmp/spack-cc-env/cc.diff; then
  echo
  cat /tmp/spack-cc-env/cc.diff
  echo
  echo "Spack compiler configuration is incorrect, see diff above!"
  exit 1
fi
if grep -q '^+.*@' /tmp/spack-cc-env/cc.diff; then
  echo
  cat /tmp/spack-cc-env/cc.diff
  echo -en '\e[1;31m'
  echo "==============================================================================="
  echo "  Spack located compilers with different versions than expected! This usually"
  echo "  means the compilers provided by the OS have updated. If this is not a bug,"
  echo "  please submit an MR updating the version number in compilers.yaml."
  echo
  echo "  Note that this will cause a rebuild of all Spack-based dependencies. To"
  echo "  speed up the rebuild process, consider notifying the Spack team to update the"
  echo "  images used for their public buildcache."
  echo "==============================================================================="
  echo -en '\e[0m'
  exit 1
fi
