#!/bin/bash -ex

VER="$1"
shift

# Splat the sources somewhere in /tmp
mkdir /tmp/python
tar -x -f /src/Python-"$VER".tar.xz -C /tmp/python --strip-components=1

# Configure the build directory
mkdir /tmp/python-build
(cd /tmp/python-build && /tmp/python/configure \
  --quiet --prefix=/opt/python-"$VER" --enable-optimizations "$@")

# Build and install to /opt
make -C /tmp/python-build -j16 >/dev/null 2>&1
make -C /tmp/python-build -j16 -s --no-print-directory install

# Strip all executables in /opt to reduce the size
# Ignore the return code for files mark executable but not ELF
find /opt/python-"$VER" -type f -executable -execdir strip '{}' + || :
strip /opt/python-"$VER"/bin/python3

# Add links for the final interpreter executable and scripts
ln -s -t /usr/local/bin/ /opt/python-"$VER"/bin/*
ln -s /opt/python-"$VER"/bin/pip3 /usr/local/bin/pip

# Tarball the binaries for extraction
tar cJf /dst/python-"$VER".tar.xz /opt/python-"$VER" /usr/local/bin/*
